// This file can be replaced during build by using the `fileReplacements` array.
// `ng build` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  firebase: {
      apiKey: "AIzaSyD6R3W0z1rHS-drjN68Wdm198fxvGgvySo",
      authDomain: "taller-final-c6fe9.firebaseapp.com",
      projectId: "taller-final-c6fe9",
      storageBucket: "taller-final-c6fe9.appspot.com",
      messagingSenderId: "248040534813",
      appId: "1:248040534813:web:da7feeff8a47089b56a7f4"
  },
  production: false
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.
