import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { ProductsService } from 'src/app/services/products.service';

@Component({
  selector: 'app-new-product',
  templateUrl: './new-product.component.html',
  styleUrls: ['./new-product.component.css']
})
export class NewProductComponent implements OnInit {

  formulario: FormGroup;

  constructor(
    private productsService: ProductsService
  ) {
    this.formulario = new FormGroup({
      budget: new FormControl(),
      unit: new FormControl(),
      description: new FormControl(),
      quantity: new FormControl(),
      unitValue: new FormControl(),
      totalValue: new FormControl(),
      acquisitionDate: new FormControl(),
      supplier: new FormControl()
    });

    this.formulario.get('unitValue')?.valueChanges.subscribe(() => {
      this.calculateTotalValue();
    });
    this.formulario.get('quantity')?.valueChanges.subscribe(() => {
      this.calculateTotalValue();
    });
  }

  ngOnInit(): void { }

  async onSubmit() {
    const formValue = this.formulario.value;
    formValue.budget = parseFloat(formValue.budget.replace(/[^0-9.-]+/g, ""));
    formValue.unitValue = parseFloat(formValue.unitValue.replace(/[^0-9.-]+/g, ""));
    formValue.totalValue = parseFloat(formValue.totalValue.replace(/[^0-9.-]+/g, ""));
    console.log(formValue);
    const response = await this.productsService.addProduct(formValue);
    console.log(response);
  }

  calculateTotalValue() {
    const quantity = this.formulario.get('quantity')?.value;
    const unitValue = parseFloat(this.formulario.get('unitValue')?.value.replace(/[^0-9.-]+/g, ""));
    if (quantity !== null && unitValue !== null) {
      this.formulario.get('totalValue')?.setValue(this.formatCurrency(quantity * unitValue));
    }
  }

  formatCurrency(value: string | number): string {
    if (typeof value === 'number') {
      value = value.toFixed(2);
    }
    return value.replace(/\B(?=(\d{3})+(?!\d))/g, ",");
  }
}

