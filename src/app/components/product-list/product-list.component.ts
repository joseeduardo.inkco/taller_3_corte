import { Component, OnInit } from '@angular/core';
import Product from 'src/app/interfaces/product.interface';
import { ProductsService } from 'src/app/services/products.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-product-list',
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.css']
})
export class ProductListComponent implements OnInit {

  products: Product[];
  selectedProduct: Product | null = null;
  editForm: FormGroup;

  constructor(
    private productsService: ProductsService,
    private formBuilder: FormBuilder
  ) {
    this.products = [];
    this.editForm = this.formBuilder.group({
      id: [''],
      budget: ['', Validators.required],
      unit: ['', Validators.required],
      description: ['', Validators.required],
      quantity: ['', Validators.required],
      unitValue: ['', Validators.required],
      totalValue: ['', Validators.required],
      acquisitionDate: ['', Validators.required],
      supplier: ['', Validators.required]
    });
  }

  ngOnInit(): void {
    this.productsService.getProducts().subscribe(products => {
      this.products = products;
    });
  }

  async onClickDelete(product: Product) {
    const response = await this.productsService.deleteProduct(product);
    console.log(response);
  }

  onClickEdit(product: Product) {
    this.selectedProduct = product;
    this.editForm.patchValue(product);
  }

  async onSubmitEdit() {
    if (this.editForm.valid) {
      const updatedProduct = this.editForm.value;
      await this.productsService.updateProduct(updatedProduct);
      this.selectedProduct = null;
      this.editForm.reset();
      // Vuelve a cargar la lista de productos
      this.productsService.getProducts().subscribe(products => {
        this.products = products;
      });
    } else {
      console.log('Formulario no válido');
    }
  }
}


