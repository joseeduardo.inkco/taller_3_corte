import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { CommonModule } from '@angular/common'; // Importa CommonModule

import { AppComponent } from './app.component';
import { NewProductComponent } from './components/new-product/new-product.component';  // Asegúrate que el nombre y ruta son correctos
import { ProductListComponent } from './components/product-list/product-list.component';  // Asegúrate que el nombre y ruta son correctos
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { AgmCoreModule } from '@agm/core';
import { initializeApp, provideFirebaseApp } from '@angular/fire/app';
import { environment } from '../environments/environment';
import { provideFirestore, getFirestore } from '@angular/fire/firestore';

@NgModule({
  declarations: [
    AppComponent,
    NewProductComponent,  // Asegúrate que el nombre es correcto
    ProductListComponent,  // Asegúrate que el nombre es correcto
  ],
  imports: [
    BrowserModule,
    ReactiveFormsModule,
    CommonModule,  // Añadir CommonModule aquí
    NgbModule,
    AgmCoreModule.forRoot({
      apiKey: 'API KEY GOOGLE MAPS'
    }),
    provideFirebaseApp(() => initializeApp(environment.firebase)),
    provideFirestore(() => getFirestore())
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
