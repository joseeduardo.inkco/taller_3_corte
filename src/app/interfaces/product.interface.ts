// src/app/interfaces/product.interface.ts
export default interface Product {
    id: number;
    budget: number; // Monto asignado
    unit: string; // Departamento responsable
    description: string; // Descripción
    quantity: number; // Número de unidades del producto
    unitValue: number; // Costo por producto
    totalValue: number; // Costo total
    acquisitionDate: Date; // Fecha de adquisición del producto
    supplier: string; // Entidad proveedora
  }
  